const searchForm = document.getElementById('search-form');
const inputForm = document.getElementById('form-input');
const cardGrid = document.querySelector('.results__grid');

class ButtonMore {
    constructor(pageNum){
        this.pageNum = 1;
    }

    createButtonMore = () => {
        const button = document.createElement('button');

        button.innerText = 'Показать больше фильмов';
        button.classList.add('more-movies');
        button.setAttribute('id', 'more-button');

        const getMoreMovies = () => {
            getMovieList(inputForm.value, ++this.pageNum);
        }

        button.addEventListener('click', () => {
            getMoreMovies();
        });

        document.querySelector(".results__grid").after(button);
    }

    deleteButtonMore = () => {
        this.pageNum = 1;
        document.getElementById('more-button').removeEventListener('click', this.getMoreMovies);
        document.getElementById('more-button').remove();
    }
}

const showMore = new ButtonMore();

document.addEventListener('DOMContentLoaded', () => checkLocalStorage());

searchForm.addEventListener('submit', (event) => {
    let inputText = inputForm.value;
    if(!localStorage.getItem(`vanilamovesearch+${inputText}`)){
        searchHistory(inputText);
    }
    if(document.getElementById('more-button')){
        showMore.deleteButtonMore();
    }
    clearGrid();
    getMovieList(inputText, 0);
    showMore.createButtonMore();
    event.preventDefault();
});


async function getMovieList(label, pageNumber){
    const searchText = label.replace(/ /g, '_');
    const url = pageNumber === 0 ? (`http://www.omdbapi.com/?s=${searchText}&apikey=d393d971`) :
    (`http://www.omdbapi.com/?s=${searchText}&page=${pageNumber}&apikey=d393d971`);

    let apiResponse = await fetch(url)
        .then(response => response.json())
        .then(data => checkData(data))
        .then(data => {
            createHeading(data.totalResults);
            data.Search.forEach(elem => {
                getMovieData(elem.imdbID).then(movie => cardGrid.appendChild(render(movie)));
            });
        })
        .catch(error => checkError(error))
}

async function getMovieData(movieId) {
    let apiResponse = await fetch("http://www.omdbapi.com/?i=" + movieId + "&apikey=d393d971")
        .then(response => response.json())
        .then(data => {
            return {
                Title: data.Title,
                Year: data.Year,
                Genre: data.Genre,
                Poster: data.Poster,
            };
        });
    return apiResponse;
}

const render = movieData => {
    const movie = document.getElementById('card').content.cloneNode(true);
    const title = movie.querySelector('.movie-title');
    const year = movie.querySelector('.movie-year');
    const genre = movie.querySelector('.movie-genre');

    title.textContent = `${movieData.Title}`;
    year.textContent = `${movieData.Year}`;
    genre.textContent = `${movieData.Genre}`;

    const poster = movie.querySelector('.movie-poster');
    if(movieData.Poster === 'N/A'){
        poster.remove();
        movie.querySelector('.movie-info').classList.replace('movie-info','movie-info-noposter');
        title.classList.replace('movie-title','movie-noposter-title');
        year.classList.replace('movie-year','movie-noposter');
        genre.classList.replace('movie-genre','movie-noposter');
    } else {
        poster.src = movieData.Poster;
    }

    return movie;
};

const searchResultNumber = number => {
    const answerList = ['фильм', 'фильма', 'фильмов'];
    number %= 100;
    if (number >= 5 && number <= 20) {
        return answerList[2];
    }

    number %= 10;
    if (number === 1) {
        return answerList[0];
    }

    if (number >= 2 && number <= 4) {
        return answerList[1];
    }

    return answerList[2];
}

const clearGrid = () => {
    const cardTemplate = document.querySelector('.results__grid');
    while(cardTemplate.firstChild){
        cardTemplate.removeChild(cardTemplate.firstChild);
    }
}

const createHeading = totalResults => {
    const heading = document.querySelector('.results__heading');
    heading.innerHTML = `<span class='heading-text'>Нашли ${totalResults} ${searchResultNumber(totalResults)}</span>`;
}

const checkError = error => {
    const heading = document.querySelector('.results__heading');
    heading.innerHTML = `<span class='heading-text'>${error.message}</span>`;
}

const checkData = data => {
    if(data.Response === 'False'){
        if(data.Error === "Movie not found!"){
            throw new Error(('Мы не поняли о чём речь ¯' + '&#92;' + '&#95;' + '(ツ)_/¯'));
        } else {
            throw new Error(data.Error);
        }
    }

    return data;
}

const searchHistory = film => {
    localStorage.setItem(`vanilamovesearch+${film}`, film);
    let timer;

    const field = document.querySelector('.form__history');
    const template = document.getElementById('search-history').content.cloneNode(true);
    const movieName = template.querySelector('.search-history-element');
    movieName.textContent = `${film}`;
    
    movieName.addEventListener('click', () => {
        console.log(timer);
        if(inputForm.value !== film && !timer){
            timer = setTimeout(() => {
                inputForm.value = film;
                if(document.getElementById('more-button')){
                    showMore.deleteButtonMore();
                }
                clearGrid();
                getMovieList(film, 0);
                showMore.createButtonMore();
                timer = undefined;
            }, 500);
        }
    }, false);

    movieName.addEventListener('dblclick', function doubleClick(){
        clearTimeout(timer);
        localStorage.removeItem(`vanilamovesearch+${film}`);
        field.removeChild(movieName);
        //movieName.removeEventListener('click', singleClick);
        movieName.removeEventListener('dblclick', doubleClick);
    });

    field.appendChild(template);
}


const checkLocalStorage = () => {
    const cache = Object.keys(localStorage);
    cache.forEach(elem => {
        if(elem.includes('vanilamovesearch+')){
            searchHistory(localStorage.getItem(elem));
        }
    })
}
