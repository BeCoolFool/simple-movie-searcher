class CurrentYear extends HTMLElement {
    constructor() {
        super();
        const shadow = this.attachShadow({
            mode: 'open'
        });
        shadow.innerHTML = `Created in TFS, ${new Date().getFullYear()}`;
    }
}
customElements.define('current-year', CurrentYear);